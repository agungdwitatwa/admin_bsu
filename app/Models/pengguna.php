<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class pengguna extends Authenticatable
{
    use HasFactory;
    protected $table = 'pgn';
    public $incrementing = false;
    protected $guarded = ['created_at'];

    public function profile()
    {
        return $this->hasOne(profile::class, 'id_pgn', 'id');
    }

    public function mahasiswa()
    {
        return $this->hasMany(mahasiswa::class, 'id_pgn', 'id');
    }
}
