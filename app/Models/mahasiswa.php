<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mahasiswa extends Model
{
    use HasFactory;
    protected $table = 'mhs';
    public $incrementing = false;
    protected $guarded = ['created_at'];

    public function akademik()
    {
        return $this->hasMany(akademik::class, 'id_mhs', 'id');
    }

    public function prestasi()
    {
        return $this->hasMany(prestasi::class, 'id_mhs', 'id');
    }
}
