<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class akademik extends Model
{
    use HasFactory;
    protected $table = 'akd';
    public $incrementing = false;
    protected $guarded = ['created_at'];
}
