<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\profile as Profile;
use App\Models\mahasiswa as Mahasiswa;
use App\Models\akademik as Akademik;
use App\Models\prestasi as Prestasi;
use App\Models\pengguna as Pengguna;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function Profil(Request $req)
    {
        $pengguna = Pengguna::find(session()->get('id'));
        $validatedData = $req->validate([
            'nama' => 'required|max:255',
            'deskripsi' => 'required',
            'alamat' => 'required',
            'email' => 'required|email',
            'telepon' => 'required|numeric',
            'website' => 'required',
            'logo' => 'required|image|file|max:1024',
            'singkatan' => 'required'
        ], [
            'required' => 'Kolom ini harus terisi',
            'logo.required' => 'Anda harus memasukkan logo',
            'telepon.numeric' => 'Nomor telepon harus berupa angka',
            'image' => 'File yang Anda masukkan harus berupa gambar',
            'max' => 'File yang Anda masukkan melebihi ukuran maksimal (maks. 1MB)'
        ]);

        $validatedData['id'] = strtr(base64_encode(Str::uuid()), '+/=', '._-');
        $validatedData['id_pgn'] = session()->get('id');
        $validatedData['instagram'] = $req->input('instagram');
        $validatedData['youtube'] = $req->input('youtube');
        $validatedData['facebook'] = $req->input('facebook');
        $validatedData['logo'] = $req->file('logo')->store('logo');

        Profile::create($validatedData);
        $pengguna->status = "old";
        $pengguna->save();
        session()->put('status', 'old');
        return redirect('/');
    }

    public function profilUpdate(Request $req)
    {
        $validatedData = $req->validate([
            'nama' => 'required|max:255',
            'deskripsi' => 'required',
            'alamat' => 'required',
            'email' => 'required|email',
            'telepon' => 'required|numeric',
            'website' => 'required',
            'logo' => 'image|file|max:1024',
            'singkatan' => 'required'
        ], [
            'nohp.numeric' => 'Nomor telepon harus berupa angka',
            'logo.image' => 'File yang Anda masukkan harus berupa gambar',
            'max' => 'File yang Anda masukkan melebihi ukuran maksimal (maks. 1MB)'
        ]);
        $validatedData['instagram'] = $req->input('instagram');
        $validatedData['youtube'] = $req->input('youtube');
        $validatedData['facebook'] = $req->input('facebook');

        if ($req->file('logo')) {
            $validatedData['logo'] = $req->file('logo')->store('logo');
            Storage::delete($req->input('oldLogo'));
        }

        Profile::where('id', $req->input('kampus'))->update($validatedData);
        return redirect('profil')->with('success', 'Profil Kampus berhasil diperbarui');
    }

    public function Mahasiswa(Request $req)
    {
        $validatedData = $req->validate([
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'alamat' => 'required',
            'nim' => 'required',
            'email' => 'required|email',
            'nohp' => 'required|numeric',
            'fakultas' => 'required',
            'jurusan' => 'required',
            'foto' => 'required|image|file|max:1024'
        ], [
            'required' => 'Kolom Ini harus terisi',
            'nohp.numeric' => 'Nomor telepon harus berupa angka',
            'foto' => 'File yang Anda masukkan harus berupa gambar',
            'max' => 'File yang Anda masukkan melebihi ukuran maksimal (maks. 1MB)'
        ]);

        $validatedData['id'] = strtr(base64_encode(Str::uuid()), '+/=', '._-');
        $validatedData['id_pgn'] = session()->get('id');
        $validatedData['foto'] = $req->file('foto')->store($validatedData['id']);

        Mahasiswa::create($validatedData);
        return redirect('/mahasiswa')->with('success', 'Mahasiswa berhasil ditambah');

        dd($validatedData);
    }

    public function mahasiswaUpdate(Request $req)
    {

        $validatedData = $req->validate([
            'tal' => 'date',
            'email' => 'email',
            'nohp' => 'numeric',
            'foto' => 'image|file|max:1024'
        ], [
            'nohp.numeric' => 'Nomor telepon harus berupa angka',
            'foto.image' => 'File yang Anda masukkan harus berupa gambar',
            'max' => 'File yang Anda masukkan melebihi ukuran maksimal (maks. 1MB)'
        ]);

        $validatedData['nama'] = $req->input('nama');
        $validatedData['jk'] = $req->input('jenis_kelamin');
        $validatedData['tel'] = $req->input('tempat_lahir');
        $validatedData['alamat'] = $req->input('alamat');
        $validatedData['nim'] = $req->input('nim');
        $validatedData['fakultas'] = $req->input('fakultas');
        $validatedData['jurusan'] = $req->input('jurusan');

        if ($req->file('foto')) {
            $validatedData['foto'] = $req->file('foto')->store($req->input('mahasiswa'));
            Storage::delete($req->input('oldImages'));
        }

        Mahasiswa::where('id', $req->input('mahasiswa'))->update($validatedData);
        return redirect('mahasiswa/' . $req->input('mahasiswa') . '/detail')->with('success', 'Data Mahasiswa berhasil diperbarui');
    }

    public function mahasiswaDelete(Request $req)
    {
        Akademik::where('id_mhs', $req->input('mahasiswa'))->delete();
        Prestasi::where('id_mhs', $req->input('mahasiswa'))->delete();
        Mahasiswa::destroy($req->input('mahasiswa'));
        Storage::deleteDirectory($req->input('mahasiswa'));
        return redirect('mahasiswa')->with('success', 'Mahasiswa berhasil dihapus');
    }

    public function Akademik(Request $req)
    {
        $validatedData = $req->validate([
            'semester' => 'required|numeric',
            'jl_sks' => 'required|numeric',
            'ip' => 'required|numeric',
            'khs' => 'required|mimes:pdf|file|max:1024',
            'krs' => 'required|mimes:pdf|file|max:1024',
        ], [
            'required' => 'Kolom Ini harus terisi',
            'numeric' => 'Nomor telepon harus berupa angka',
            'khs.mimes' => 'File yang Anda masukkan harus berformat PDF (.pdf)',
            'krs.mimes' => 'File yang Anda masukkan harus berformat PDF (.pdf)',
            'max' => 'File yang Anda masukkan melebihi ukuran maksimal (maks. 1MB)'
        ]);
        $validatedData['id'] = strtr(base64_encode(Str::uuid()), '+/=', '._-');
        $validatedData['id_pgn'] = session()->get('id');
        $validatedData['id_mhs'] = $req->input('mahasiswa');
        $validatedData['khs'] = $req->file('khs')->store($validatedData['id_mahasiswa']);
        $validatedData['krs'] = $req->file('krs')->store($validatedData['id_mahasiswa']);

        Akademik::create($validatedData);
        return redirect('mahasiswa/' . $req->input('mahasiswa') . '/akademik')->with('success', 'Data Akademik berhasil ditambah');
    }

    public function Prestasi(Request $req)
    {
        $validatedData = $req->validate([
            'nm_prs' => 'required',
            'tingkat' => 'required',
            'jenis' => 'required',
            'sertifikat' => 'required|mimes:pdf|file|max:1024',
        ], [
            'required' => 'Kolom Ini harus terisi',
            'numeric' => 'Nomor telepon harus berupa angka',
            'sertifikat.mimes' => 'File sertifikat yang Anda masukkan harus berformat PDF (.pdf)',
            'max' => 'File yang Anda masukkan melebihi ukuran maksimal (maks. 1MB)'
        ]);
        $validatedData['id'] = strtr(base64_encode(Str::uuid()), '+/=', '._-');
        $validatedData['id_pgn'] = session()->get('id');
        $validatedData['id_mhs'] = $req->input('mahasiswa');
        $validatedData['sertifikat'] = $req->file('sertifikat')->store($validatedData['id_mahasiswa']);

        Prestasi::create($validatedData);
        return redirect('mahasiswa/' . $req->input('mahasiswa') . '/prestasi')->with('success', 'Data prestasi mahasiswa berhasil ditambah');
    }

    public function deleteAkademik(Request $req)
    {
        Akademik::destroy($req->input('akademik'));
        Storage::delete($req->input('krs'));
        Storage::delete($req->input('khs'));
        return redirect('mahasiswa/' . $req->input('mahasiswa') . '/akademik')->with('success', 'Data Akademik berhasil dihapus');
    }

    public function deletePrestasi(Request $req)
    {
        Prestasi::destroy($req->input('prestasi'));
        Storage::delete($req->input('sertifikat'));
        return redirect('mahasiswa/' . $req->input('mahasiswa') . '/prestasi')->with('success', 'Data Prestasi berhasil dihapus');
    }
}
