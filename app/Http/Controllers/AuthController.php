<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\pengguna as Pengguna;

class AuthController extends Controller
{
    public function Login(Request $req)
    {
        $credentials = $req->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt($credentials)) {
            $req->session()->regenerate();
            $req->session()->put('id', Pengguna::select('id')->where('username', $credentials['username'])->first()->id);
            $req->session()->put('status', Pengguna::select('status')->where('username', $credentials['username'])->first()->status);
            return redirect()->intended('/new/profil');
        }
    }

    public function Logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
