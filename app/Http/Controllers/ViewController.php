<?php

namespace App\Http\Controllers;

use App\Models\mahasiswa as Mahasiswa;
use App\Models\pengguna as Pengguna;
use App\Models\akademik as Akademik;

class ViewController extends Controller
{
    public function Landing()
    {
        $data = [
            'profil' => Pengguna::find(session()->get('id'))->profile,
            'mahasiswa' => Pengguna::find(session()->get('id'))->mahasiswa()->orderBy('nama', 'asc')->get()
        ];

        return view('dashboard', $data);
    }
    public function Profil()
    {
        $profil = Pengguna::find(session()->get('id'))->profile;
        return view('profil', $profil);
    }

    public function Mahasiswa()
    {
        $data = [
            'mahasiswa' => Pengguna::find(session()->get('id'))->mahasiswa()->orderBy('nama', 'asc')->get()
        ];
        return view('mahasiswa', $data);
    }

    public function Akademik($id_mahasiswa)
    {
        $data = [
            'data' => Mahasiswa::find($id_mahasiswa),
            'akademik' => Mahasiswa::find($id_mahasiswa)->akademik
        ];
        return view('akademik_mahasiswa', $data);
    }

    public function Prestasi($id_mahasiswa)
    {
        $data = [
            'data' => Mahasiswa::find($id_mahasiswa),
            'prestasi' => Mahasiswa::find($id_mahasiswa)->prestasi
        ];
        return view('prestasi_mahasiswa', $data);
    }

    public function Detail($id_mahasiswa)
    {
        $data = [
            'data' => Mahasiswa::find($id_mahasiswa),
            'akademik' => Mahasiswa::find($id_mahasiswa)->akademik,
            'prestasi' => Mahasiswa::find($id_mahasiswa)->prestasi,
        ];
        return view('detail_mahasiswa', $data);
    }
    public function Login()
    {
        return view('login');
    }

    public function newProfil()
    {
        if (session()->get('status') != 'new') {
            return redirect('profil');
        }
        return view('profil-new');
    }
}
