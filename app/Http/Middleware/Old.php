<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Old
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (session()->get('status') == 'new') {
            return redirect('new/profil');
        }
        return $next($request);
    }
}
