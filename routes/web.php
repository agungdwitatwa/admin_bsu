<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ViewController::class, 'Landing'])->middleware(['auth', 'old']);

Route::get('/profil', [ViewController::class, 'Profil'])->middleware(['auth', 'old']);
Route::get('/mahasiswa', [ViewController::class, 'Mahasiswa'])->middleware(['auth', 'old']);
Route::get('/mahasiswa/{id_mahasiswa}/akademik', [ViewController::class, 'Akademik'])->middleware(['auth', 'old']);
Route::get('/mahasiswa/{id_mahasiswa}/prestasi', [ViewController::class, 'Prestasi'])->middleware(['auth', 'old']);
Route::get('/mahasiswa/{id_mahasiswa}/detail', [ViewController::class, 'Detail'])->middleware(['auth', 'old']);
Route::get('/new/profil', [ViewController::class, 'newProfil'])->middleware('auth');

// POST DATA
Route::post('/profil', [PostController::class, 'Profil'])->middleware('auth');
Route::post('/profil/update', [PostController::class, 'profilUpdate'])->middleware('auth');
Route::post('/mahasiswa', [PostController::class, 'Mahasiswa'])->middleware('auth');
Route::post('/mahasiswa/update', [PostController::class, 'mahasiswaUpdate'])->middleware('auth');
Route::post('/mahasiswa/delete', [PostController::class, 'mahasiswaDelete'])->middleware('auth');
Route::post('/akademik', [PostController::class, 'Akademik'])->middleware('auth');
Route::post('/akademik/delete', [PostController::class, 'deleteAkademik'])->middleware('auth');
Route::post('/prestasi', [PostController::class, 'Prestasi'])->middleware('auth');
Route::post('/prestasi/delete', [PostController::class, 'deletePrestasi'])->middleware('auth');

//Autentikasi
Route::post('/login', [AuthController::class, 'Login'])->middleware('guest');
Route::get('/login', [ViewController::class, 'Login'])->name('login')->middleware('guest');
Route::post('/logout', [AuthController::class, 'Logout'])->middleware('auth');
