<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\pengguna as Pengguna;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Pengguna::create([
            'id' => strtr(base64_encode(Str::uuid()), '+/=', '._-'),
            'username' => 'agung',
            'password' => Hash::make('12345'),
            'status' => 'new'
        ]);
    }
}
