<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkademiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akademiks', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('id_pgn');
            $table->foreign('id_pgn')->references('id')->on('penggunas');
            $table->string('id_mhs');
            $table->foreign('id_mhs')->references('id')->on('mahasiswas');
            $table->string('semester');
            $table->string('jl_sks');
            $table->string('ip');
            $table->string('khs');
            $table->string('krs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akademiks');
    }
}
