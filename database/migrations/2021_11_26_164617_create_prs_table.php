<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrestasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestasis', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('id_pgn');
            $table->foreign('id_pgn')->references('id')->on('penggunas');
            $table->string('id_mhs');
            $table->foreign('id_mhs')->references('id')->on('mahasiswas');
            $table->string('nm_prs');
            $table->string('jenis');
            $table->string('tingkat');
            $table->string('sertifikat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestasis');
    }
}
