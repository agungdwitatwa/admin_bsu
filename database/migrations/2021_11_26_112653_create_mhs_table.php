<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('id_pgn');
            $table->foreign('id_pgn')->references('id')->on('penggunas');
            $table->string('nama');
            $table->string('jk');
            $table->string('tel');
            $table->string('tal');
            $table->string('alamat');
            $table->string('nim');
            $table->string('email');
            $table->string('nohp');
            $table->string('fakultas');
            $table->string('jurusan');
            $table->string('foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
