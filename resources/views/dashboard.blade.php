<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Beasiswa NTB, Beasiswa Miskin Berprestasi, BMB, BMB NTB">
    <meta name="description" content="Admin Beasiswa Miskin Berprestasi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="A.D.A.M">
    <title>BMB | Beasiswa Miskin Berprestasi</title>
    <meta name="robots" content="noindex">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
        rel="stylesheet">
    <link type="text/css" href="{{ url('vendor/spinkit.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('vendor/perfect-scrollbar.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/material-icons.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/fontawesome.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/preloader.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/app.css') }}" rel="stylesheet">
</head>

<body class="layout-sticky-subnav layout-learnly ">
    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>
    <div class="mdk-header-layout js-mdk-header-layout">
        <div id="header" class="mdk-header js-mdk-header mb-0" data-fixed data-effects="waterfall">
            <div class="mdk-header__content">
                <div class="navbar navbar-expand navbar-light bg-white border-bottom" id="default-navbar" data-primary>
                    <div class="container page__container">
                        <a href="{{ url('') }}" class="navbar-brand mr-16pt">

                            <span class="avatar avatar-sm navbar-brand-icon mr-0 mr-lg-8pt">

                                <span class="avatar-title rounded bg-primary"><img
                                        src="{{ url('images/illustration/student/128/white.svg') }}" alt="logo"
                                        class="img-fluid" /></span>

                            </span>

                            <span class="d-none d-lg-block" style="font-size: 15pt">Beasiswa NTB</span>
                        </a>
                        <ul class="nav navbar-nav ml-auto mr-0">
                            <li class="nav-item"><i class="material-icons">lock_open</i>
                            </li>
                            <li class="nav-item">
                                <form action="{{ url('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-secondary">Logout</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- // END Header -->

        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content page-content ">

            <div class="page-section bg-alt border-bottom-2">
                <div class="container page__container">

                    <div class="d-flex flex-column flex-lg-row align-items-center">
                        <div
                            class="d-flex flex-column flex-md-row align-items-center flex mb-16pt mb-lg-0 text-center text-md-left">
                            <div class="mb-16pt mb-md-0 mr-md-24pt">
                                <img src="{{ asset('storage/' . $profil->logo) }}" width="104" alt="teacher">
                            </div>
                            <div class="flex">
                                <h1 class="h2 mb-0">{{ $profil->nama }}</h1>
                            </div>
                        </div>
                        <div class="ml-lg-16pt">
                            <a href="{{ url('profil') }}" data-target="#library-drawer" data-toggle="sidebar"
                                class="btn btn-light">
                                <i class="material-icons icon--left">tune</i> Profil
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="page-section">
                <div class="container page__container">
                    <div class="row card-group-row mb-8pt">
                        <div class="col-sm-6 card-group-row__col">
                            <div class="card card-sm card-group-row__card">
                                <div class="card-body d-flex align-items-start p-4">
                                    <div class="flex">
                                        <h4>Deskripsi Kampus</h4>
                                        <div class="d-flex align-items-start ">
                                            <div class="rating mr-8pt">
                                                {{ $profil->deskripsi }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 card-group-row__col">
                            <div class="card card-sm card-group-row__card">
                                <div class="card-body d-flex align-items-start p-4">
                                    <div class="flex">
                                        <h4>Alamat Kampus</h4>
                                        <div class="d-flex align-items-start mb-3">
                                            <div class="rating mr-8pt">
                                                {{ $profil->alamat }}
                                                <br>
                                                Email : {{ $profil->email }}
                                                <br>
                                                Telepon : {{ $profil->telepon }}
                                            </div>
                                        </div>
                                        <h4>Sosial Media</h4>
                                        <div class="d-flex align-items-center">
                                            <a href="{{ $profil->facebook }}"
                                                class="text-accent fab fa-facebook-square font-size-24pt mr-8pt"></a>
                                            <a href="{{ $profil->instagram }}"
                                                class="text-accent fab fa-instagram font-size-24pt mr-8pt"></a>
                                            <a href="{{ $profil->youtube }}"
                                                class="text-accent fab fa-youtube font-size-24pt mr-8pt"></a>
                                            <a href="{{ $profil->website }}"
                                                class="text-accent fas fa-globe font-size-24pt mr-8pt"></a>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="mb-4 d-flex justify-content-between align-items-center"
                                style="text-align:right">
                                <h5 class="mb-0">Daftar Penerima Beasiswa</h5>
                                <a class="btn btn-outline-secondary" href="{{ url('mahasiswa') }}"> <i
                                        class="fas fa-user-graduate"></i> &nbsp; Mahasiswa</a>
                            </div>

                            <div class="row">
                                @foreach ($mahasiswa as $m)
                                    <div class="col-sm-6 col-xl-3">

                                        <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary js-overlay mdk-reveal js-mdk-reveal "
                                            data-partial-height="44" data-toggle="popover" data-trigger="click">
                                            <a href="instructor-edit-course.html" class="js-image"
                                                data-position="">
                                                <img src="{{ asset('storage/' . $m->foto) }}" alt="course"
                                                    width="400" height="300">
                                                <span class="overlay__content align-items-start justify-content-start">
                                                </span>
                                            </a>
                                            <div class="mdk-reveal__content">
                                                <div class="card-body">
                                                    <div class="d-flex">
                                                        <div class="flex">
                                                            <a class="card-title mb-4pt"
                                                                href="instructor-edit-course.html"
                                                                style="text-align: center">{{ $m->nama }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="popoverContainer d-none">
                                            <div class="media">
                                                <div class="media-left mr-12pt">
                                                    <img src="{{ asset('storage/' . $m->foto) }}" width="40"
                                                        height="60" alt="Angular" class="rounded">
                                                </div>
                                                <div class="media-body">
                                                    <div class="card-title mb-0">{{ $m->nama }}</div>
                                                    <p class="lh-1">
                                                        <span class="text-50 small">NIM : {{ $m->nim }}</span>
                                                    </p>
                                                </div>
                                            </div>

                                            <p class="my-16pt text-70">{{ $m->alamat }}</p>

                                            <div class="mb-16pt">
                                                <div class="d-flex align-items-center">
                                                    <span
                                                        class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                    <p class="flex text-50 lh-1 mb-0">
                                                        <small>{{ $m->email }}</small>
                                                    </p>
                                                </div>
                                                <div class="d-flex align-items-center">
                                                    <span
                                                        class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                    <p class="flex text-50 lh-1 mb-0">
                                                        <small>{{ $m->jenis_kelamin }}</small>
                                                    </p>
                                                </div>
                                                <div class="d-flex align-items-center">
                                                    <span
                                                        class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                    <p class="flex text-50 lh-1 mb-0">
                                                        <small>{{ $m->nohp }}</small>
                                                    </p>
                                                </div>
                                                <div class="d-flex align-items-center">
                                                    <span
                                                        class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                    <p class="flex text-50 lh-1 mb-0"><small>{{ $m->jurusan }}
                                                        </small>
                                                    </p>
                                                </div>
                                            </div>
                                            <p>
                                                <a href="{{ url('mahasiswa/' . $m->id . '/detail') }}"
                                                    class="btn btn-success btn-sm">
                                                    <i class="fas fa-info-circle"></i>
                                                    &nbsp; Detail Data</a>
                                            </p>
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- // END Header Layout Content -->

        <!-- Footer -->

        <div class="bg-dark mt-auto">
            <div class="container page__container page-section d-flex flex-column align-items-center text-center">
                <img src="{{ url('images/illustration/student/128/white.svg') }}" alt="" width="50">
                <p class="text-white-70 brand mb-24pt mt-20pt">
                    BEASISWA NTB DALAM NEGERI
                </p>
                <p class="measure-lead-max text-white-50 small mr-8pt">Beasiswa NTB Dalam Negeri merupakan bantuan dana
                    pendidikan berupa beasiswa yang diberikan kepada masyarakat/mahasiswa asal Nusa Tenggara
                    Barat yang menjadi mahasiswa di Perguruan Tinggi di dalam Negeri</p>
                <p class="mb-8pt d-flex">
                    <a href="https://beasiswa.ntbprov.go.id/"
                        class="text-white-70 text-underline mr-8pt small">Website</a>
                    <a href="https://www.instagram.com/beasiswantb_dalamnegeri/"
                        class="text-white-70 text-underline mr-8pt small">Instagram</a>
                    <a href="https://www.facebook.com/beasiswantbdalamnegeri"
                        class="text-white-70 text-underline mr-8pt small">Facebook</a>
                </p>
                <p class="text-white-50 small mt-n1 mb-0">Copyright 2021 © All rights reserved.</p>
            </div>
        </div>

        <!-- // END Footer -->

    </div>

    <script src="{{ url('vendor/jquery.min.js') }}"></script>
    <script src="{{ url('vendor/popper.min.js') }}"></script>
    <script src="{{ url('vendor/bootstrap.min.js') }}"></script>
    <script src="{{ url('vendor/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ url('vendor/dom-factory.js') }}"></script>
    <script src="{{ url('vendor/material-design-kit.js') }}"></script>
    <script src="{{ url('js/app.js') }}"></script>
    <script src="{{ url('js/preloader.js') }}"></script>

</body>

</html>
