<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Beasiswa NTB, Beasiswa Miskin Berprestasi, BMB, BMB NTB">
    <meta name="description" content="Admin Beasiswa Miskin Berprestasi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="A.D.A.M">
    <title>BMB | Profil</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
        rel="stylesheet">

    <!-- Preloader -->
    <link type="text/css" href="{{ url('vendor/spinkit.css') }}" rel="stylesheet">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{ url('vendor/perfect-scrollbar.css') }}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{ url('css/material-icons.css') }}" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link type="text/css" href="{{ url('css/fontawesome.css') }}" rel="stylesheet">

    <!-- Preloader -->
    <link type="text/css" href="{{ url('css/preloader.css') }}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{ url('css/app.css') }}" rel="stylesheet">

    <style>
        .logo {
            width: 100%;
            border: dashed 1px #999;
        }

        .logo input {
            display: none;
        }

        .logo label {
            display: block;
            width: 100%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            cursor: pointer;
            margin-bottom: 0px;
        }

        .logo label:hover {
            color: #999;
        }

        .preview-logo {
            display: none;
            padding: 5px;
            border: dashed 1px #999;
            margin-bottom: 10px;
        }

    </style>
</head>

<body class="layout-sticky-subnav layout-learnly ">

    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>

    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">

        <!-- Header -->

        <div id="header" class="mdk-header js-mdk-header mb-0" data-fixed data-effects="waterfall">
            <div class="mdk-header__content">

                <div class="navbar navbar-expand navbar-light bg-white border-bottom" id="default-navbar" data-primary>
                    <div class="container page__container">

                        <!-- Navbar Brand -->
                        <a href="{{ url('') }}" class="navbar-brand mr-16pt">

                            <span class="avatar avatar-sm navbar-brand-icon mr-0 mr-lg-8pt">

                                <span class="avatar-title rounded bg-primary"><img
                                        src="{{ url('images/illustration/student/128/white.svg') }}" alt="logo"
                                        class="img-fluid" /></span>

                            </span>

                            <span class="d-none d-lg-block" style="font-size: 15pt">Beasiswa NTB</span>
                        </a>
                        <ul class="nav navbar-nav ml-auto mr-0">
                            <li class="nav-item"><i class="material-icons">lock_open</i>
                            </li>
                            <li class="nav-item">
                                <form action="{{ url('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-secondary">Logout</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- // END Header -->

        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content page-content ">

            <div class="page-section bg-alt border-bottom-2">
                <div class="container page__container">

                    <div class="d-flex flex-column flex-lg-row align-items-center">
                        <div
                            class="d-flex flex-column flex-md-row align-items-center flex mb-16pt mb-lg-0 text-center text-md-left">
                            <div class="flex">
                                <h1 class="h2 mb-0">Profil Kampus/Universitas</h1>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="page-section">
                <div class="container page__container">
                    @if (session()->has('success'))
                        <div class="alert bg-success text-white border-0" role="alert">
                            <div class="d-flex flex-wrap align-items-start">
                                <div class="mr-8pt">
                                    <i class="material-icons">access_time</i>
                                </div>
                                <div class="flex" style="min-width: 180px">
                                    <small>
                                        <strong>Sukses !</strong> {{ session('success') }}
                                    </small>
                                </div>
                            </div>
                        </div>
                    @endif
                    <form action="{{ url('/profil') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row mb-32pt">
                                    <div class="col-lg-4">
                                        <div class="page-separator">
                                            <div class="page-separator__text">Profil Kampus/Universitas</div>
                                        </div>
                                        <div class="col-md-12 p-4 mb-4" style="border:1px solid #999">
                                            <h3>Logo</h3>
                                            @error('logo')
                                                <div class="alert bg-danger text-white border-0" role="alert">
                                                    <div class="d-flex flex-wrap align-items-start">
                                                        <div class="mr-8pt">
                                                            <i class="material-icons">access_time</i>
                                                        </div>
                                                        <div class="flex" style="min-width: 180px">
                                                            <small>
                                                                <strong>Error !</strong> {{ $message }}
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @enderror
                                            <div class="preview-logo">
                                                {{-- <img src="{{ asset('storage/' . $logo) }}" id="logo-preview" --}}
                                                <img id="logo-preview" width="100%">
                                            </div>
                                            <div class="logo">
                                                <label for="logo">Upload Logo</label>
                                                <input type="file" id="logo" name="logo" onchange="showPreview(event)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 d-flex">
                                        <div class="flex" style="max-width: 100%">
                                            <div class="card m-0">
                                                <div class="col-lg-12 p-2 pt-4">
                                                    <div class="row px-2">
                                                        <!-- left -->
                                                        <div class="col-lg-8">
                                                            <div class="form-group">
                                                                <label class="form-label" for="nama">Nama
                                                                    Kampus</label>
                                                                <input type="text"
                                                                    class="form-control @error('nama') is-invalid  @enderror"
                                                                    id="nama" name="nama" value="{{ old('nama') }}">
                                                                @error('nama')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group">
                                                                <label class="form-label" for="singkatan">Nama
                                                                    Singkatan</label>
                                                                <input type="text"
                                                                    class="form-control @error('singkatan') is-invalid  @enderror"
                                                                    id="singkatan" name="singkatan"
                                                                    value="{{ old('singkatan') }}">
                                                                @error('singkatan')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label class="form-label" for="deskripsi">Deskripsi
                                                                    Kampus</label>
                                                                <textarea
                                                                    class="form-control @error('deskripsi') is-invalid  @enderror"
                                                                    name="deskripsi" id="" cols="30"
                                                                    rows="5">{{ old('deskripsi') }}</textarea>
                                                                @error('deskripsi')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="alamat">Alamat</label>
                                                                <input type="text"
                                                                    class="form-control @error('alamat') is-invalid  @enderror"
                                                                    id="alamat" name="alamat"
                                                                    value="{{ old('alamat') }}">
                                                                @error('alamat')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="email">Email</label>
                                                                <input type="email"
                                                                    class="form-control @error('email') is-invalid  @enderror"
                                                                    id="email" name="email"
                                                                    value="{{ old('email') }}">
                                                                @error('email')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="telepon">Telepon</label>
                                                                <input type="number"
                                                                    class="form-control @error('telepon') is-invalid  @enderror"
                                                                    id="telepon" name="telepon"
                                                                    value="{{ old('telepon') }}">
                                                                @error('telepon')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="website">Website</label>
                                                                <input type="text"
                                                                    class="form-control @error('website') is-invalid  @enderror"
                                                                    id="website" name="website"
                                                                    value="{{ old('website') }}">
                                                                @error('website')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="instagram">Instagram</label>
                                                                <input type="text" class="form-control" id="instagram"
                                                                    name="instagram" value="{{ old('instagram') }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="facebook">Facebook</label>
                                                                <input type="text" class="form-control" id="facebook"
                                                                    name="facebook" value="{{ old('facebook') }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="youtube">Youtube</label>
                                                                <input type="text" class="form-control" id="youtube"
                                                                    name="youtube" value="{{ old('youtube') }}">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="mt-4" style="text-align:center">
                                                <button type="submit" class="btn btn-primary">Submit Data</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- // END Header Layout Content -->

        <!-- Footer -->

        <div class="bg-dark mt-auto">
            <div class="container page__container page-section d-flex flex-column align-items-center text-center">
                <img src="{{ url('images/illustration/student/128/white.svg') }}" alt="" width="50">
                <p class="text-white-70 brand mb-24pt mt-20pt">
                    BEASISWA NTB DALAM NEGERI
                </p>
                <p class="measure-lead-max text-white-50 small mr-8pt">Beasiswa NTB Dalam Negeri merupakan bantuan dana
                    pendidikan berupa beasiswa yang diberikan kepada masyarakat/mahasiswa asal Nusa Tenggara
                    Barat yang menjadi mahasiswa di Perguruan Tinggi di dalam Negeri</p>
                <p class="mb-8pt d-flex">
                    <a href="https://beasiswa.ntbprov.go.id/"
                        class="text-white-70 text-underline mr-8pt small">Website</a>
                    <a href="https://www.instagram.com/beasiswantb_dalamnegeri/"
                        class="text-white-70 text-underline mr-8pt small">Instagram</a>
                    <a href="https://www.facebook.com/beasiswantbdalamnegeri"
                        class="text-white-70 text-underline mr-8pt small">Facebook</a>
                </p>
                <p class="text-white-50 small mt-n1 mb-0">Copyright 2021 © All rights reserved.</p>
            </div>
        </div>

        <!-- // END Footer -->

    </div>
    <!-- // END Header Layout -->

    <!-- jQuery -->
    <script>
        function showPreview(event) {
            if (event.target.files.length > 0) {
                let src = URL.createObjectURL(event.target.files[0]);
                let preview = document.getElementById("logo-preview");
                let previewBox = document.querySelector(".preview-logo");
                preview.src = src;
                preview.style.display = "block";
                previewBox.style.display = "block";
            }
        }
    </script>
    <script src="{{ url('vendor/jquery.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ url('vendor/popper.min.js') }}"></script>
    <script src="{{ url('vendor/bootstrap.min.js') }}"></script>

    <!-- Perfect Scrollbar -->
    <script src="{{ url('vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- DOM Factory -->
    <script src="{{ url('vendor/dom-factory.js') }}"></script>

    <!-- MDK -->
    <script src="{{ url('vendor/material-design-kit.js') }}"></script>

    <!-- App JS -->
    <script src="{{ url('js/app.js') }}"></script>

    <!-- Preloader -->
    <script src="{{ url('js/preloader.js') }}"></script>

</body>

</html>
