<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Beasiswa NTB, Beasiswa Miskin Berprestasi, BMB, BMB NTB">
    <meta name="description" content="Admin Beasiswa Miskin Berprestasi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="A.D.A.M">
    <title>BMB | Mahasiswa</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
        rel="stylesheet">

    <!-- Preloader -->
    <link type="text/css" href="{{ url('vendor/spinkit.css') }}" rel="stylesheet">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{ url('vendor/perfect-scrollbar.css') }}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{ url('css/material-icons.css') }}" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link type="text/css" href="{{ url('css/fontawesome.css') }}" rel="stylesheet">

    <!-- Preloader -->
    <link type="text/css" href="{{ url('css/preloader.css') }}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{ url('css/app.css') }}" rel="stylesheet">
    <style>
        .foto-mahasiswa {
            width: 100%;
            border: dashed 1px #999;
        }

        .foto-mahasiswa input {
            display: none;
        }

        .foto-mahasiswa label {
            display: block;
            width: 100%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            cursor: pointer;
            margin-bottom: 0px;
        }

        .foto-mahasiswa label:hover {
            color: #999;
        }

        .preview-foto-mahasiswa {
            display: none;
            padding: 5px;
            border: dashed 1px #999;
            margin-bottom: 10px;
        }

    </style>

</head>

<body class="layout-sticky-subnav layout-learnly ">

    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>

        <!-- <div class="sk-bounce">
    <div class="sk-bounce-dot"></div>
    <div class="sk-bounce-dot"></div>
  </div> -->

        <!-- More spinner examples at https://github.com/tobiasahlin/SpinKit/blob/master/examples.html -->
    </div>

    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">

        <!-- Header -->

        <div id="header" class="mdk-header js-mdk-header mb-0" data-fixed data-effects="waterfall">
            <div class="mdk-header__content">

                <div class="navbar navbar-expand navbar-light bg-white border-bottom" id="default-navbar" data-primary>
                    <div class="container page__container">

                        <!-- Navbar Brand -->
                        <a href="{{ url('') }}" class="navbar-brand mr-16pt">

                            <span class="avatar avatar-sm navbar-brand-icon mr-0 mr-lg-8pt">

                                <span class="avatar-title rounded bg-primary"><img
                                        src="{{ url('images/illustration/student/128/white.svg') }}" alt="logo"
                                        class="img-fluid" /></span>

                            </span>

                            <span class="d-none d-lg-block" style="font-size: 15pt">Beasiswa NTB</span>
                        </a>
                        <ul class="nav navbar-nav ml-auto mr-0">
                            <li class="nav-item"><i class="material-icons">lock_open</i>
                            </li>
                            <li class="nav-item">
                                <form action="{{ url('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-secondary">Logout</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- // END Header -->

        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content page-content ">

            <div class="page-section bg-alt border-bottom-2">
                <div class="container page__container">

                    <div class="d-flex flex-column flex-lg-row align-items-center">
                        <div
                            class="d-flex flex-column flex-md-row align-items-center flex mb-16pt mb-lg-0 text-center text-md-left">
                            <div class="flex">
                                <h4 class="h4 mb-0">Universitas Teknologi Sumbawa > Mahasiswa
                                </h4>
                            </div>
                        </div>
                        <div class="ml-lg-16pt">
                            <a href="{{ url('') }}" class="btn btn-light"> <span
                                    class="material-icons">open_in_new</span>
                                &nbsp; Kembali</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="page-section">
                <div class="container page__container">
                    @if (session()->has('success'))
                        <div class="alert bg-success text-white border-0" role="alert">
                            <div class="d-flex flex-wrap align-items-start">
                                <div class="mr-8pt">
                                    <i class="material-icons">access_time</i>
                                </div>
                                <div class="flex" style="min-width: 180px">
                                    <small>
                                        <strong>Sukses !</strong> {{ session('success') }}
                                    </small>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="mb-4 d-flex justify-content-between align-items-center" style="text-align:right">
                        <h5 class="mb-0">Daftar Penerima Beasiswa</h5>
                        <a class="btn btn-outline-secondary" href="#collapseExample" data-toggle="collapse">Tambah
                            Data</a>
                    </div>
                    <div class="collapse @if ($errors->any()) show  @endif" id="collapseExample">
                        <div class="card card-body p-5">
                            <form action="{{ url('/mahasiswa') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="page-separator">
                                            <div class="page-separator__text">Data Diri</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="col-md-12 p-4 mb-4" style="border:1px solid #999">
                                            <h3>Foto Mahasiswa</h3>
                                            @error('foto')
                                                <div class="alert bg-danger text-white border-0" role="alert">
                                                    <div class="d-flex flex-wrap align-items-start">
                                                        <div class="mr-8pt">
                                                            <i class="material-icons">access_time</i>
                                                        </div>
                                                        <div class="flex" style="min-width: 180px">
                                                            <small>
                                                                <strong>Error !</strong> {{ $message }}
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @enderror
                                            <div class="preview-foto-mahasiswa">
                                                <img id="foto-mahasiswa-preview" width="100%">
                                            </div>
                                            <div class="foto-mahasiswa">
                                                <label for="foto">Upload Foto</label>
                                                <input type="file" id="foto" name="foto" onchange="showPreview(event)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 d-flex mb-3">
                                        <div class="flex" style="max-width: 100%">
                                            <div class="card m-0">
                                                <div class="col-lg-12 p-2 pt-4">
                                                    <div class="row px-2">
                                                        <!-- left -->
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="nama">Nama
                                                                    Lengkap Mahasiswa
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control  @error('nama') is-invalid  @enderror"
                                                                    id="nama" name="nama" value="{{ old('nama') }}">
                                                                @error('nama')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="jk">Jenis
                                                                    Kelamin</label>
                                                                <select name="jk" id="jk"
                                                                    class="form-control  @error('jk') is-invalid  @enderror">
                                                                    @if (old('jk') == 'Laki-Laki')
                                                                        <option value="Laki-Laki" selected> Laki -
                                                                            Laki</option>
                                                                        <option value="Perempuan"> Perempuan
                                                                        </option>
                                                                    @else
                                                                        <option value="Laki-Laki"> Laki -
                                                                            Laki</option>
                                                                        <option value="Perempuan" selected>
                                                                            Perempuan
                                                                        </option>
                                                                    @endif
                                                                </select>
                                                                @error('jk')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="tel">Tempat
                                                                    Lahir</label>
                                                                <input type="text"
                                                                    class="form-control  @error('tel') is-invalid  @enderror"
                                                                    id="tel" name="tel" value="{{ old('tel') }}">
                                                                @error('tel')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="tal">Tanggal
                                                                    Lahir</label>
                                                                <input type="date"
                                                                    class="form-control  @error('tal') is-invalid  @enderror"
                                                                    id="tal" name="tal" value="{{ old('tal') }}">
                                                                @error('tal')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="alamat">Alamat</label>
                                                                <input type="text"
                                                                    class="form-control  @error('alamat') is-invalid  @enderror"
                                                                    id="alamat" name="alamat"
                                                                    value="{{ old('alamat') }}">
                                                                @error('alamat')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="nim">NIM
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control  @error('nim') is-invalid  @enderror"
                                                                    id="nim" name="nim" value="{{ old('nim') }}">
                                                                @error('nim')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="email">Email
                                                                </label>
                                                                <input type="email"
                                                                    class="form-control  @error('email') is-invalid  @enderror"
                                                                    id="email" name="email"
                                                                    value="{{ old('email') }}">
                                                                @error('email')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="nohp">No.
                                                                    HP </label>
                                                                <input type="number"
                                                                    class="form-control  @error('nohp') is-invalid  @enderror"
                                                                    id="nohp" name="nohp" value="{{ old('nohp') }}">
                                                                @error('nohp')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="fakultas">Fakultas
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control  @error('fakultas') is-invalid  @enderror"
                                                                    id="fakultas" name="fakultas"
                                                                    value="{{ old('fakultas') }}">
                                                                @error('fakultas')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="jurusan">Jurusan
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control  @error('jurusan') is-invalid  @enderror"
                                                                    id="jurusan" name="jurusan"
                                                                    value="{{ old('jurusan') }}">
                                                                @error('jurusan')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4" style="text-align:center">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="page-separator"></div>
                    <div class="row">

                        @foreach ($mahasiswa as $m)
                            <div class="d-flex flex-column col-sm-6 col-xl-2 align-items-end">
                                <form action="{{ url('mahasiswa/delete') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="mahasiswa" value="{{ $m->id }}">
                                    <button class="btn btn-danger btn-sm "
                                        style="position: relative; top:22px; right:1px; z-index:100;">
                                        <i class="fas fa-trash"></i></button>
                                </form>
                                <div class="col-sm-12 col-xl-12 px-0">
                                    <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary js-overlay mdk-reveal js-mdk-reveal"
                                        data-partial-height="44" data-toggle="popover" data-trigger="click">
                                        <a href="instructor-edit-course.html" class="js-image" data-position="">
                                            <img src="{{ asset('storage/' . $m->foto) }}" alt="course" width="400"
                                                height="300">
                                            <span class="overlay__content align-items-start justify-content-start">
                                            </span>
                                        </a>
                                        <div class="mdk-reveal__content">
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="flex">
                                                        <a class="card-title mb-4pt" href="instructor-edit-course.html"
                                                            style="text-align: center"> {{ $m->nama }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="popoverContainer d-none">
                                        <div class="media mb-3">
                                            <div class="media-left mr-12pt">
                                                <img src="{{ asset('storage/' . $m->foto) }}" width="40" height="60"
                                                    alt="Angular" class="rounded">
                                            </div>
                                            <div class="media-body">
                                                <div class="card-title mb-0">{{ $m->nama }}</div>
                                                <p class="lh-1">
                                                    <span class="text-50 small">NIM : {{ $m->nim }}</span>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="mb-16pt">
                                            <div class="d-flex align-items-center">
                                                <span
                                                    class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                <p class="flex text-50 lh-1 mb-0"><small>{{ $m->email }}</small>
                                                </p>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <span
                                                    class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                <p class="flex text-50 lh-1 mb-0">
                                                    <small>{{ $m->jenis_kelamin }}</small>
                                                </p>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <span
                                                    class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                <p class="flex text-50 lh-1 mb-0"><small>{{ $m->nohp }}</small>
                                                </p>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <span
                                                    class="material-icons icon-16pt text-50 mr-8pt">arrow_right_alt</span>
                                                <p class="flex text-50 lh-1 mb-0"><small>{{ $m->jurusan }}</small>
                                                </p>
                                            </div>
                                            <p class=" mt-16pt text-70">
                                                <a href="{{ url('mahasiswa/' . $m->id . '/akademik') }}"
                                                    class="btn btn-warning btn-sm">Akademik</a>
                                                <a href="{{ url('mahasiswa/' . $m->id . '/prestasi') }}"
                                                    class="btn btn-success btn-sm">Prestasi</a>
                                                <a href="{{ url('mahasiswa/' . $m->id . '/detail') }}"
                                                    class="btn btn-primary btn-sm">Detail</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
        <!-- // END Header Layout Content -->

        <!-- Footer -->

        <div class="bg-dark mt-auto">
            <div class="container page__container page-section d-flex flex-column align-items-center text-center">
                <img src="{{ url('images/illustration/student/128/white.svg') }}" alt="" width="50">
                <p class="text-white-70 brand mb-24pt mt-20pt">
                    BEASISWA NTB DALAM NEGERI
                </p>
                <p class="measure-lead-max text-white-50 small mr-8pt">Beasiswa NTB Dalam Negeri merupakan bantuan dana
                    pendidikan berupa beasiswa yang diberikan kepada masyarakat/mahasiswa asal Nusa Tenggara
                    Barat yang menjadi mahasiswa di Perguruan Tinggi di dalam Negeri</p>
                <p class="mb-8pt d-flex">
                    <a href="https://beasiswa.ntbprov.go.id/"
                        class="text-white-70 text-underline mr-8pt small">Website</a>
                    <a href="https://www.instagram.com/beasiswantb_dalamnegeri/"
                        class="text-white-70 text-underline mr-8pt small">Instagram</a>
                    <a href="https://www.facebook.com/beasiswantbdalamnegeri"
                        class="text-white-70 text-underline mr-8pt small">Facebook</a>
                </p>
                <p class="text-white-50 small mt-n1 mb-0">Copyright 2021 © All rights reserved.</p>
            </div>
        </div>

        <!-- // END Footer -->

    </div>
    <!-- // END Header Layout -->

    <!-- jQuery -->
    <script>
        function showPreview(event) {
            if (event.target.files.length > 0) {
                let src = URL.createObjectURL(event.target.files[0]);
                let preview = document.getElementById("foto-mahasiswa-preview");
                let previewBox = document.querySelector(".preview-foto-mahasiswa");
                preview.src = src;
                preview.style.display = "block";
                previewBox.style.display = "block";
            }
        }
    </script>
    <script src="{{ url('vendor/jquery.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ url('vendor/popper.min.js') }}"></script>
    <script src="{{ url('vendor/bootstrap.min.js') }}"></script>

    <!-- Perfect Scrollbar -->
    <script src="{{ url('vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- DOM Factory -->
    <script src="{{ url('vendor/dom-factory.js') }}"></script>

    <!-- MDK -->
    <script src="{{ url('vendor/material-design-kit.js') }}"></script>

    <!-- App JS -->
    <script src="{{ url('js/app.js') }}"></script>

    <!-- Preloader -->
    <script src="{{ url('js/preloader.js') }}"></script>

</body>

</html>
