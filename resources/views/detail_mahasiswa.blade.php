<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Beasiswa NTB, Beasiswa Miskin Berprestasi, BMB, BMB NTB">
    <meta name="description" content="Admin Beasiswa Miskin Berprestasi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="A.D.A.M">
    <title>Manage Courses</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
        rel="stylesheet">
    <link type="text/css" href="{{ url('vendor/spinkit.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('vendor/perfect-scrollbar.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/material-icons.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/fontawesome.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/preloader.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/app.css') }}" rel="stylesheet">

    <style>
        .logo {
            width: 100%;
            border: dashed 1px #999;
        }

        .logo input {
            display: none;
        }

        .logo label {
            display: block;
            width: 100%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            cursor: pointer;
            margin-bottom: 0px;
        }

        .logo label:hover {
            color: #999;
        }

        .preview-logo {
            padding: 5px;
            border: dashed 1px #999;
            margin-bottom: 10px;
        }

    </style>

</head>

<body class="layout-sticky-subnav layout-learnly ">

    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>

    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">

        <!-- Header -->

        <div id="header" class="mdk-header js-mdk-header mb-0" data-fixed data-effects="waterfall">
            <div class="mdk-header__content">

                <div class="navbar navbar-expand navbar-light bg-white border-bottom" id="default-navbar" data-primary>
                    <div class="container page__container">

                        <!-- Navbar Brand -->
                        <a href="{{ url('') }}" class="navbar-brand mr-16pt">

                            <span class="avatar avatar-sm navbar-brand-icon mr-0 mr-lg-8pt">

                                <span class="avatar-title rounded bg-primary"><img
                                        src="{{ url('images/illustration/student/128/white.svg') }}" alt="logo"
                                        class="img-fluid" /></span>

                            </span>

                            <span class="d-none d-lg-block" style="font-size: 15pt">Beasiswa NTB</span>
                        </a>
                        <ul class="nav navbar-nav ml-auto mr-0">
                            <li class="nav-item"><i class="material-icons">lock_open</i>
                            </li>
                            <li class="nav-item">
                                <form action="{{ url('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-secondary">Logout</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- // END Header -->

        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content page-content ">

            <div class="page-section bg-alt border-bottom-2">
                <div class="container page__container">

                    <div class="d-flex flex-column flex-lg-row align-items-center">
                        <div
                            class="d-flex flex-column flex-md-row align-items-center flex mb-16pt mb-lg-0 text-center text-md-left">
                            <div class="flex">
                                <h4 class="h4 mb-0">Mahasiswa > {{ $data->nama }}
                                </h4>
                            </div>
                        </div>
                        <div class="ml-lg-16pt">
                            <a href="{{ url('mahasiswa') }}" class="btn btn-light"> <span
                                    class="material-icons">open_in_new</span>
                                &nbsp; Kembali</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="page-section">
                <div class="container page__container">
                    @if (session()->has('success'))
                        <div class="alert bg-success text-white border-0" role="alert">
                            <div class="d-flex flex-wrap align-items-start">
                                <div class="mr-8pt">
                                    <i class="material-icons">access_time</i>
                                </div>
                                <div class="flex" style="min-width: 180px">
                                    <small>
                                        <strong>Sukses !</strong> {{ session('success') }}
                                    </small>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ url('mahasiswa/update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="page-separator">
                                    <div class="page-separator__text">Data Diri</div>
                                </div>
                                <input type="hidden" name="oldImages" value="{{ $data->foto }}">
                                <div class="row mb-32pt">
                                    <div class="col-lg-4">
                                        <div class="col-md-12 p-4 mb-4" style="border:1px solid #999">
                                            <h3>Foto</h3>
                                            @error('foto')
                                                <div class="alert bg-danger text-white border-0 mt-3" role="alert">
                                                    <div class="d-flex flex-wrap align-items-start">
                                                        <div class="mr-8pt">
                                                            <i class="material-icons">access_time</i>
                                                        </div>
                                                        <div class="flex" style="min-width: 180px">
                                                            <small>
                                                                <strong>Error !</strong>
                                                                {{ $message }}
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            @enderror
                                            <div class="preview-logo">
                                                <img src="{{ asset('storage') }}/{{ $data->foto }}"
                                                    id="logo-preview" width="100%">
                                            </div>
                                            <div class="logo">
                                                <label for="foto">Ganti Foto</label>
                                                <input type="file" id="foto" name="foto" onchange="showPreview(event)">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 d-flex">
                                        <div class="flex" style="max-width: 100%">
                                            <div class="card m-0">
                                                <div class="col-lg-12 p-2 pt-4">
                                                    <div class="row px-2">
                                                        <!-- left -->
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="nama">Nama
                                                                    Lengkap Mahasiswa
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control @error('nama') is-invalid  @enderror"
                                                                    id="nama" name="nama" value="{{ $data->nama }}">
                                                                @error('nama')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="jenis_kelamin">Jenis
                                                                    Kelamin</label>
                                                                <select name="jenis_kelamin" id="jenis_kelamin"
                                                                    class="form-control  @error('jenis_kelamin') is-invalid  @enderror">
                                                                    @if ($data->jenis_kelamin == 'Laki-Laki')
                                                                        <option value="Laki-Laki" selected> Laki -
                                                                            Laki</option>
                                                                        <option value="Perempuan"> Perempuan
                                                                        </option>
                                                                    @else
                                                                        <option value="Laki-Laki"> Laki -
                                                                            Laki</option>
                                                                        <option value="Perempuan" selected>
                                                                            Perempuan
                                                                        </option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="tempat_lahir">Tempat
                                                                    Lahir</label>
                                                                <input type="text"
                                                                    class="form-control @error('tempat_lahir') is-invalid  @enderror"
                                                                    id="tempat_lahir" name="tempat_lahir"
                                                                    value="{{ $data->tempat_lahir }}">
                                                                @error('tempat_lahir')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="tanggal_lahir">Tanggal
                                                                    Lahir</label>
                                                                <input type="date"
                                                                    class="form-control  @error('tanggal_lahir') is-invalid  @enderror"
                                                                    id="tanggal_lahir" name="tanggal_lahir"
                                                                    value="{{ $data->tanggal_lahir }}">
                                                                @error('tanggal_lahir')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label"
                                                                    for="alamat">Alamat</label>
                                                                <input type="text"
                                                                    class="form-control @error('alamat') is-invalid  @enderror"
                                                                    id="alamat" name="alamat"
                                                                    value="{{ $data->alamat }}">
                                                                @error('alamat')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="nim">NIM
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control @error('nim') is-invalid  @enderror"
                                                                    id="nim" name="nim" value="{{ $data->nim }}">
                                                                @error('nim')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="email">Email
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control @error('email') is-invalid  @enderror"
                                                                    id="email" name="email"
                                                                    value="{{ $data->email }}">
                                                                @error('email')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="nohp">
                                                                    No.HP
                                                                </label>
                                                                <input type="number"
                                                                    class="form-control @error('nohp') is-invalid  @enderror"
                                                                    id="nohp" name="nohp" value="{{ $data->nohp }}">
                                                                @error('nohp')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="form-label" for="fakultas">
                                                                    Fakultas
                                                                </label>
                                                                <input type="text"
                                                                    class="form-control @error('fakultas') is-invalid  @enderror"
                                                                    id="fakultas" name="fakultas"
                                                                    value="{{ $data->fakultas }}">
                                                                @error('fakultas')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    class="form-label @error('jurusan') is-invalid  @enderror"
                                                                    for="jurusan">
                                                                    Jurusan
                                                                </label>
                                                                <input type="text" class="form-control" id="jurusan"
                                                                    name="jurusan" value="{{ $data->jurusan }}">
                                                                @error('jurusan')
                                                                    <div class="invalid-feedback">
                                                                        {{ $message }}
                                                                    </div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4" style="text-align:center">
                                                <button type="submit" class="btn btn-primary" name="mahasiswa"
                                                    value="{{ $data->id }}">Update Data</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-11">
                            <div class="page-separator">
                                <div class="page-separator__text">Data Akademik</div>
                            </div>
                        </div>
                        <div class="col-lg-1 d-flex justify-content-end b-1">
                            <a href="{{ url('mahasiswa/' . $data->id . '/akademik') }}" type="submit"
                                class="btn btn-warning btn-sm" style="margin-bottom: 16px"> <i
                                    class="fas fa-edit    "></i> &nbsp; Edit</a>
                        </div>
                        <div class="col-lg-12">
                            <div class="card mb-lg-32pt">
                                <div class="table-responsive" data-toggle="lists"
                                    data-lists-values="[&quot;js-lists-values-name&quot;]">

                                    <table
                                        class="table table-bordered table-flush mb-0 thead-border-top-0 table-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="border-left-0">
                                                    Semester
                                                </th>
                                                <th class="border-left-0">
                                                    Jumlah SKS
                                                </th>
                                                <th class="border-left-0">
                                                    IP
                                                </th>
                                                <th>
                                                    <div class="lh-1 d-flex flex-column text-50 my-4pt">
                                                        File KRS
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="lh-1 d-flex flex-column text-50 my-4pt">
                                                        File KHS
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="list" id="contacts">
                                            @foreach ($akademik as $ak)
                                                <tr>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $ak->semester }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $ak->jumlah_sks }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $ak->ip }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <a class="d-flex flex-column border-1 rounded bg-light px-8pt py-4pt lh-1"
                                                            href="{{ asset('storage/' . $ak->krs) }}"
                                                            target="_blank">
                                                            <small>
                                                                <strong class="js-lists-values-name text-black-100">
                                                                    File KRS
                                                                </strong>
                                                            </small>
                                                            <small class="text-black-50"><i
                                                                    class="fas fa-file    "></i>
                                                                &nbsp;
                                                                KRS - {{ $data->nama }}</small>
                                                        </a>

                                                    </td>
                                                    <td>

                                                        <a class="d-flex flex-column border-1 rounded bg-light px-8pt py-4pt lh-1"
                                                            href="{{ asset('storage/' . $ak->krs) }}"
                                                            target="_blank">
                                                            <small>
                                                                <strong class="js-lists-values-name text-black-100">
                                                                    File KHS
                                                                </strong>
                                                            </small>
                                                            <small class="text-black-50"><i
                                                                    class="fas fa-file    "></i>
                                                                &nbsp;
                                                                KHS - {{ $data->nama }}</small>
                                                        </a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-11">
                            <div class="page-separator">
                                <div class="page-separator__text">Data Prestasi</div>
                            </div>
                        </div>
                        <div class="col-lg-1 d-flex justify-content-end b-1">
                            <a href="{{ url('mahasiswa/' . $data->id . '/prestasi') }}" type="submit"
                                class="btn btn-success btn-sm" style="margin-bottom: 16px"> <i
                                    class="fas fa-edit    "></i> &nbsp; Edit</a>
                        </div>
                        <div class="col-lg-12">
                            <div class="card mb-lg-32pt">
                                <div class="table-responsive" data-toggle="lists"
                                    data-lists-values="[&quot;js-lists-values-name&quot;]">

                                    <table
                                        class="table table-bordered table-flush mb-0 thead-border-top-0 table-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="border-left-0">
                                                    Nama Prestasi
                                                </th>
                                                <th class="border-left-0">
                                                    Jenis Prestasi
                                                </th>
                                                <th class="border-left-0">
                                                    Tingkat Prestasi
                                                </th>
                                                <th>
                                                    <div class="lh-1 d-flex flex-column text-50 my-4pt">
                                                        Sertifikat
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="list" id="contacts">
                                            @foreach ($prestasi as $p)
                                                <tr>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $p->nama_prestasi }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $p->jenis }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $p->tingkat }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <a class="d-flex flex-column border-1 rounded bg-light px-8pt py-4pt lh-1"
                                                            href="{{ asset('storage/' . $p->sertifikat) }}"
                                                            target="_blank">
                                                            <small>
                                                                <strong class="js-lists-values-name text-black-100">
                                                                    File Sertifikat
                                                                </strong>
                                                            </small>
                                                            <small class="text-black-50"><i
                                                                    class="fas fa-file    "></i>
                                                                &nbsp;
                                                                Sertifikat - {{ $data->nama }}</small>
                                                        </a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- // END Header Layout Content -->

        <!-- Footer -->

        <div class="bg-dark mt-auto">
            <div class="container page__container page-section d-flex flex-column align-items-center text-center">
                <img src="{{ url('images/illustration/student/128/white.svg') }}" alt="" width="50">
                <p class="text-white-70 brand mb-24pt mt-20pt">
                    BEASISWA NTB DALAM NEGERI
                </p>
                <p class="measure-lead-max text-white-50 small mr-8pt">Beasiswa NTB Dalam Negeri merupakan bantuan dana
                    pendidikan berupa beasiswa yang diberikan kepada masyarakat/mahasiswa asal Nusa Tenggara
                    Barat yang menjadi mahasiswa di Perguruan Tinggi di dalam Negeri</p>
                <p class="mb-8pt d-flex">
                    <a href="https://beasiswa.ntbprov.go.id/"
                        class="text-white-70 text-underline mr-8pt small">Website</a>
                    <a href="https://www.instagram.com/beasiswantb_dalamnegeri/"
                        class="text-white-70 text-underline mr-8pt small">Instagram</a>
                    <a href="https://www.facebook.com/beasiswantbdalamnegeri"
                        class="text-white-70 text-underline mr-8pt small">Facebook</a>
                </p>
                <p class="text-white-50 small mt-n1 mb-0">Copyright 2021 © All rights reserved.</p>
            </div>
        </div>

        <!-- // END Footer -->

    </div>
    <!-- // END Header Layout -->

    <!-- jQuery -->
    <script>
        function showPreview(event) {
            if (event.target.files.length > 0) {
                let src = URL.createObjectURL(event.target.files[0]);
                let preview = document.getElementById("logo-preview");
                preview.src = src;
                preview.style.display = "block";
            }
        }
    </script>
    <script src="{{ url('vendor/jquery.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ url('vendor/popper.min.js') }}"></script>
    <script src="{{ url('vendor/bootstrap.min.js') }}"></script>

    <!-- Perfect Scrollbar -->
    <script src="{{ url('vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- DOM Factory -->
    <script src="{{ url('vendor/dom-factory.js') }}"></script>

    <!-- MDK -->
    <script src="{{ url('vendor/material-design-kit.js') }}"></script>

    <!-- App JS -->
    <script src="{{ url('js/app.js') }}"></script>

    <!-- Preloader -->
    <script src="{{ url('js/preloader.js') }}"></script>

</body>

</html>
