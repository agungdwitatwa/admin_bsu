<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Beasiswa NTB, Beasiswa Miskin Berprestasi, BMB, BMB NTB">
    <meta name="description" content="Admin Beasiswa Miskin Berprestasi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="A.D.A.M">
    <title>BMB | Akademik</title>
    <meta name="robots" content="noindex">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
        rel="stylesheet">
    <link type="text/css" href="{{ url('vendor/spinkit.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('vendor/perfect-scrollbar.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/material-icons.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/fontawesome.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/preloader.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ url('css/app.css') }}" rel="stylesheet">

    <style>
        .foto-mahasiswa {
            width: 100%;
            border: dashed 1px #999;
        }

        .foto-mahasiswa input {
            display: none;
        }

        .foto-mahasiswa label {
            display: block;
            width: 100%;
            height: 40px;
            line-height: 40px;
            text-align: center;
            cursor: pointer;
            margin-bottom: 0px;
        }

        .foto-mahasiswa label:hover {
            color: #999;
        }

        .preview-foto-mahasiswa {
            padding: 5px;
            border: dashed 1px #999;
            margin-bottom: 10px;
        }

    </style>

</head>

<body class="layout-sticky-subnav layout-learnly ">

    <div class="preloader">
        <div class="sk-chase">
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
            <div class="sk-chase-dot"></div>
        </div>
    </div>
    <div class="mdk-header-layout js-mdk-header-layout">
        <div id="header" class="mdk-header js-mdk-header mb-0" data-fixed data-effects="waterfall">
            <div class="mdk-header__content">
                <div class="navbar navbar-expand navbar-light bg-white border-bottom" id="default-navbar" data-primary>
                    <div class="container page__container">
                        <a href="{{ url('') }}" class="navbar-brand mr-16pt">
                            <span class="avatar avatar-sm navbar-brand-icon mr-0 mr-lg-8pt">
                                <span class="avatar-title rounded bg-primary"><img
                                        src="{{ url('images/illustration/student/128/white.svg') }}" alt="logo"
                                        class="img-fluid" />
                                </span>
                            </span>
                            <span class="d-none d-lg-block" style="font-size: 15pt">Beasiswa NTB</span>
                        </a>
                        <ul class="nav navbar-nav ml-auto mr-0">
                            <li class="nav-item"><i class="material-icons">lock_open</i>
                            </li>
                            <li class="nav-item">
                                <form action="{{ url('logout') }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-outline-secondary">Logout</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="mdk-header-layout__content page-content ">
            <div class="page-section bg-alt border-bottom-2">
                <div class="container page__container">
                    <div class="d-flex flex-column flex-lg-row align-items-center">
                        <div
                            class="d-flex flex-column flex-md-row align-items-center flex mb-16pt mb-lg-0 text-center text-md-left">
                            <div class="mb-16pt mb-md-0 mr-md-24pt">
                                <img src="{{ asset('storage/' . $data->foto) }}" width="104" alt="teacher">
                            </div>
                            <div class="flex">
                                <h4 class="h4 mb-0">
                                    {{ $data->nama }}</span>
                                </h4>
                                <p>{{ $data->fakultas }} - {{ $data->jurusan }}</p>
                            </div>
                        </div>
                        <div class="ml-lg-16pt">
                            <a href="{{ url('mahasiswa') }}" class="btn btn-light"> <span
                                    class="material-icons">open_in_new</span>
                                &nbsp; Kembali</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="page-section">
                <div class="container page__container">
                    @if (session()->has('success'))
                        <div class="alert bg-success text-white border-0" role="alert">
                            <div class="d-flex flex-wrap align-items-start">
                                <div class="mr-8pt">
                                    <i class="material-icons">access_time</i>
                                </div>
                                <div class="flex" style="min-width: 180px">
                                    <small>
                                        <strong>Sukses !</strong> {{ session('success') }}
                                    </small>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="mb-4 d-flex justify-content-between align-items-center"
                                style="text-align:right">
                                <h5 class="mb-0">Data Akademik</h5>
                                <a class="btn btn-outline-secondary" href="#collapseExample"
                                    data-toggle="collapse">Tambah
                                    Data</a>
                            </div>
                            <div class="collapse @if ($errors->any()) show  @endif mb-3" id="collapseExample">
                                <!-- Tab panes -->
                                <form action="{{ url('akademik') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="flex" style="max-width: 100%">
                                        <div class="card p-relative o-hidden mb-0">
                                            <div class="card-body text-70 pb-4">
                                                <input type="hidden" name="mahasiswa" value="{{ $data->id }}">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="home" role="tabpanel"
                                                        aria-labelledby="home-tab">
                                                        <div class="row col-xl-12 p-4">
                                                            <div class="col-xl-6">
                                                                <div class=" row col-xl-12">
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label"
                                                                                for="semester">Semester</label>
                                                                            <input type="number"
                                                                                class="form-control @error('semester') is-invalid  @enderror"
                                                                                id="semester" name="semester"
                                                                                value="{{ old('semester') }}">
                                                                            @error('semester')
                                                                                <div class="invalid-feedback">
                                                                                    {{ $message }}
                                                                                </div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label"
                                                                                for="jl_sks">Jumlah
                                                                                SKS</label>
                                                                            <input type="number"
                                                                                class="form-control @error('jl_sks') is-invalid  @enderror"
                                                                                id="jl_sks" name="jl_sks"
                                                                                value="{{ old('jl_sks') }}">
                                                                            @error('jl_sks')
                                                                                <div class="invalid-feedback">
                                                                                    {{ $message }}
                                                                                </div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row col-xl-12 px-4">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="ip">IP
                                                                            (Skala 4)</label>
                                                                        <input type="number"
                                                                            class="form-control @error('ip') is-invalid  @enderror"
                                                                            id="ip" name="ip"
                                                                            value="{{ old('ip') }}">
                                                                        @error('ip')
                                                                            <div class="invalid-feedback">
                                                                                {{ $message }}
                                                                            </div>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <div class="col-lg-12 mb-2">
                                                                    <div class="col-md-12 p-2"
                                                                        style="border:1px solid #999">
                                                                        <h3 style="margin:0px">KHS</h3>
                                                                        <div class="khs-mahasiswa">
                                                                            <input type="file" id="khs-mahasiswa"
                                                                                name="khs">
                                                                            @error('khs')
                                                                                <div class="alert bg-danger text-white border-0 mt-3"
                                                                                    role="alert">
                                                                                    <div
                                                                                        class="d-flex flex-wrap align-items-start">
                                                                                        <div class="mr-8pt">
                                                                                            <i
                                                                                                class="material-icons">access_time</i>
                                                                                        </div>
                                                                                        <div class="flex"
                                                                                            style="min-width: 180px">
                                                                                            <small>
                                                                                                <strong>Error !</strong>
                                                                                                {{ $message }}
                                                                                            </small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 mb-2">
                                                                    <div class="col-md-12 p-2"
                                                                        style="border:1px solid #999">
                                                                        <h3 style="margin:0px">KRS</h3>
                                                                        <div class="khs-mahasiswa">
                                                                            <input type="file" id="khs-mahasiswa"
                                                                                name="krs">
                                                                            @error('krs')
                                                                                <div class="alert bg-danger text-white border-0 mt-3"
                                                                                    role="alert">
                                                                                    <div
                                                                                        class="d-flex flex-wrap align-items-start">
                                                                                        <div class="mr-8pt">
                                                                                            <i
                                                                                                class="material-icons">access_time</i>
                                                                                        </div>
                                                                                        <div class="flex"
                                                                                            style="min-width: 180px">
                                                                                            <small>
                                                                                                <strong>Error !</strong>
                                                                                                {{ $message }}
                                                                                            </small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 my-4" style="text-align: center">
                                            <button type="submit" class="btn btn-primary">Submit Data</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="page-separator"></div>
                            <div class="card mb-lg-32pt">

                                <div class="table-responsive" data-toggle="lists"
                                    data-lists-values="[&quot;js-lists-values-name&quot;]">

                                    <table
                                        class="table table-bordered table-flush mb-0 thead-border-top-0 table-nowrap">
                                        <thead>
                                            <tr>
                                                <th class="border-left-0">
                                                    Semester
                                                </th>
                                                <th class="border-left-0">
                                                    Jumlah SKS
                                                </th>
                                                <th class="border-left-0">
                                                    IP
                                                </th>
                                                <th>
                                                    <div class="lh-1 d-flex flex-column text-50 my-4pt">
                                                        File KRS
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="lh-1 d-flex flex-column text-50 my-4pt">
                                                        File KHS
                                                    </div>
                                                </th>
                                                <th class="border-left-0" style="text-align: center">
                                                    Aksi
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="list" id="contacts">
                                            @foreach ($akademik as $ak)
                                                <tr>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $ak->semester }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $ak->jl_sks }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="border-left-0">
                                                        <div class="media flex-nowrap align-items-center"
                                                            style="white-space: nowrap;">
                                                            <div class="media-body ml-4pt">
                                                                <p class="mb-0">
                                                                    <strong class="js-lists-values-name">
                                                                        {{ $ak->ip }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <a class="d-flex flex-column border-1 rounded bg-light px-8pt py-4pt lh-1"
                                                            href="{{ asset('storage/' . $ak->krs) }}"
                                                            target="_blank">
                                                            <small>
                                                                <strong class="js-lists-values-name text-black-100">
                                                                    File KRS
                                                                </strong>
                                                            </small>
                                                            <small class="text-black-50"><i
                                                                    class="fas fa-file    "></i>
                                                                &nbsp;
                                                                KRS - {{ $data->nama }}</small>
                                                        </a>

                                                    </td>
                                                    <td>

                                                        <a class="d-flex flex-column border-1 rounded bg-light px-8pt py-4pt lh-1"
                                                            href="{{ asset('storage/' . $ak->krs) }}"
                                                            target="_blank">
                                                            <small>
                                                                <strong class="js-lists-values-name text-black-100">
                                                                    File KHS
                                                                </strong>
                                                            </small>
                                                            <small class="text-black-50"><i
                                                                    class="fas fa-file    "></i>
                                                                &nbsp;
                                                                KHS - {{ $data->nama }}</small>
                                                        </a>

                                                    </td>
                                                    <td style="text-align: center">
                                                        <form action="{{ url('akademik/delete') }}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="akademik"
                                                                value="{{ $ak->id }}">
                                                            <input type="hidden" name="krs"
                                                                value="{{ $ak->krs }}">
                                                            <input type="hidden" name="khs"
                                                                value="{{ $ak->khs }}">
                                                            <input type="hidden" name="mahasiswa"
                                                                value="{{ $data->id }}">
                                                            <button type="submit" class="btn btn-danger">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="bg-dark mt-auto">
            <div class="container page__container page-section d-flex flex-column align-items-center text-center">
                <img src="{{ url('images/illustration/student/128/white.svg') }}" alt="" width="50">
                <p class="text-white-70 brand mb-24pt mt-20pt">
                    BEASISWA NTB DALAM NEGERI
                </p>
                <p class="measure-lead-max text-white-50 small mr-8pt">Beasiswa NTB Dalam Negeri merupakan bantuan dana
                    pendidikan berupa beasiswa yang diberikan kepada masyarakat/mahasiswa asal Nusa Tenggara
                    Barat yang menjadi mahasiswa di Perguruan Tinggi di dalam Negeri</p>
                <p class="mb-8pt d-flex">
                    <a href="https://beasiswa.ntbprov.go.id/"
                        class="text-white-70 text-underline mr-8pt small">Website</a>
                    <a href="https://www.instagram.com/beasiswantb_dalamnegeri/"
                        class="text-white-70 text-underline mr-8pt small">Instagram</a>
                    <a href="https://www.facebook.com/beasiswantbdalamnegeri"
                        class="text-white-70 text-underline mr-8pt small">Facebook</a>
                </p>
                <p class="text-white-50 small mt-n1 mb-0">Copyright 2021 © All rights reserved.</p>
            </div>
        </div>
    </div>
    <script>
        function showPreview(event) {
            if (event.target.files.length > 0) {
                let src = URL.createObjectURL(event.target.files[0]);
                let preview = document.getElementById("foto-mahasiswa-preview");
                preview.src = src;
                preview.style.display = "block";
            }
        }
    </script>

    <script src="{{ url('vendor/jquery.min.js') }}"></script>
    <script src="{{ url('vendor/popper.min.js') }}"></script>
    <script src="{{ url('vendor/bootstrap.min.js') }}"></script>
    <script src="{{ url('vendor/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ url('vendor/dom-factory.js') }}"></script>
    <script src="{{ url('vendor/material-design-kit.js') }}"></script>
    <script src="{{ url('js/app.js') }}"></script>
    <script src="{{ url('js/preloader.js') }}"></script>

</body>

</html>
