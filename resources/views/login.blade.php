<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Beasiswa NTB, Beasiswa Miskin Berprestasi, BMB, BMB NTB">
    <meta name="description" content="Admin Beasiswa Miskin Berprestasi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="A.D.A.M">
    <title>Login</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
        rel="stylesheet">

    <!-- Preloader -->
    <link type="text/css" href="{{ url('vendor/spinkit.css') }}" rel="stylesheet">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{ url('vendor/perfect-scrollbar.css') }}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{ url('css/material-icons.css') }}" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link type="text/css" href="{{ url('css/fontawesome.css') }}" rel="stylesheet">

    <!-- Preloader -->
    <link type="text/css" href="{{ url('css/preloader.css') }}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{ url('css/app.css') }}" rel="stylesheet">

</head>

<body class="layout-default layout-login-centered-boxed">

    <div class="layout-login-centered-boxed__form card">
        <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
            <a href="#anchor" class="navbar-brand flex-column mb-2 align-items-center mr-0" style="min-width: 0">

                <span class="avatar avatar-sm navbar-brand-icon mr-0">

                    <span class="avatar-title rounded bg-primary"><img
                            src="{{ url('/images/illustration/student/128/white.svg') }}" alt="logo"
                            class="img-fluid" /></span>

                </span>

                BMBNTB
            </a>
            <p class="m-0">Beasiswa Miskin Berprestasi NTB</p>
        </div>

        <form action="{{ url('login') }}" method="POST">
            @csrf
            <div class="form-group">
                <label class="text-label" for="username">Email Address:</label>
                <div class="input-group input-group-merge">
                    <input id="username" name="username" type="text" class="form-control form-control-prepended"
                        placeholder="Masukkan Username" required>
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="far fa-user"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="text-label" for="password">Password:</label>
                <div class="input-group input-group-merge">
                    <input id="password" name="password" type="password" class="form-control form-control-prepended"
                        placeholder="Masukkan password Anda" required>
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-key"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit">Login</button>
            </div>
            <div class="form-group text-center">
                <a href="">Forgot password?</a>
            </div>
        </form>
    </div>

    <!-- jQuery -->
    <script src="{{ url('vendor/jquery.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ url('vendor/popper.min.js') }}"></script>
    <script src="{{ url('vendor/bootstrap.min.js') }}"></script>

    <!-- Perfect Scrollbar -->
    <script src="{{ url('vendor/perfect-scrollbar.min.js') }}"></script>

    <!-- DOM Factory -->
    <script src="{{ url('vendor/dom-factory.js') }}"></script>

    <!-- MDK -->
    <script src="{{ url('vendor/material-design-kit.js') }}"></script>

    <!-- App JS -->
    <script src="{{ url('js/app.js') }}"></script>

    <!-- Preloader -->
    <script src="{{ url('js/preloader.js') }}"></script>

</body>

</html>
